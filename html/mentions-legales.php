<?php
session_start();

include('head.php');
?>

<body>
	<div id="layout">
	<link rel="stylesheet" type="text/css" href="../css/mentions-legales.css">
		<center><h1 id="title" class="block">Mentions légales</h1></center>
	<div class="block">
		<ul><h2>I° Images et icônes :</h2>
			<li>Logo GitLab :<br>
			appartenant à GitLab, cette ressource n'a pas été modifiée.</li>
			<li>Logo LinkedIn :<br>
			Cette ressource n'est pas le logo original. La source est <a class="link" href="" >IconFinder</a>. Un fond de la même couleur que la ressource permet d'en modifier le rendu.</li>
		</ul>
	</div>
	<div class="block">
		<ul><h2>II° Solution d'hébergement</h2>
			<li>L'hébergement est fourni par <a class="link" href="truc.php">Lien à compléter</a>.</li>
		</ul>
	</div class="block">
	<div class="block">
		<ul><h2>III° Présentation fonctionalités du site :</h2>
			<li>La présentation du site a été entièrement designée et produite par moi-même.<br>
			Les fonctionalités du site web ont été réalisées par moi-même, à l'aide des technologies présentées sur la première page.<br>
			L'utilisation de librairies, frameworks ou templates est possible à l'avenir.</li>
		</ul>
	</div>
	<div class="block">
		<ul><h2>IV° Travaux présentés :</h2>
			<li>Tous les travaux présentés ont été produits par moi-même.
			Certains ont été designés par mes supérieurs en entreprise, d'autres par le réseau <a class="link" href="truc.php">CERTA</a>.</li>
		</ul>
	</div>
	<div class="block">
		<ul><h2>V° Données personnelles :</h2>
			Lorsque vous rendez sur le site, je récupère automatiquement certaines données :
			<li>Votre adresse IP publique ainsi que votre navigateur me permettent de vous proposer du contenu adapté.<br>
			Aucun lien permanent ne sera jamais créé entre ces données et les autres données que vous me partagez. Elles sont automatiquement supprimées au bout d'une durée maximale d'une heure après que vous ayez quitté le site.</li>
			<li>Votre login, votre mot de passe et voter adresse mail vous permettent de vous connecter au site. Ceci me permet d'adapter encore plus le contenu ou de répondre à un message que vous avez envoyé. Ces données sont stockées indéfiniment.</li>
			<li>Les messages que vous m'envoyez sont stockés indéfiniment. J'y répondrai, ou non dans les délais qui me conviennent.</li>
			En aucun cas je ne cède ou ne céderai les données que vous partagez à travers ce site à un tiers. Ces données ne sont et ne seront jamais utilisées à des fins commerciales.
		</ul>
	</div>
</div>
</body>
<?php
include('foot.php');