<?php
session_start();
include('head.php');
?>

<body class="body">
	<link rel="stylesheet" type="text/css" href="../css/welcome.css">
<main class="layout">
	<br>
	<div class="lblocks">
		<div class="block">
			<h2>Mon parcours vous intéresse?</h2>
			<p>
				
			</p>
		</div>
		<div class="block">
			<h2>Test</h2>
			<p>
				Nihil est enim virtute amabilius, nihil quod magis adliciat ad diligendum, quippe cum propter virtutem et probitatem etiam eos, quos numquam vidimus, quodam modo diligamus. Quis est qui C. Fabrici, M'. Curi non cum caritate aliqua benevola memoriam usurpet, quos numquam viderit? quis autem est, qui Tarquinium Superbum, qui Sp. Cassium, Sp. Maelium non oderit? Cum duobus ducibus de imperio in Italia est decertatum, Pyrrho et Hannibale; ab altero propter probitatem eius non nimis alienos animos habemus, alterum propter crudelitatem semper haec civitas oderit.
				<br>
				Quapropter a natura mihi videtur potius quam ab indigentia orta amicitia, applicatione magis animi cum quodam sensu amandi quam cogitatione quantum illa res utilitatis esset habitura. Quod quidem quale sit, etiam in bestiis quibusdam animadverti potest, quae ex se natos ita amant ad quoddam tempus et ab eis ita amantur ut facile earum sensus appareat. Quod in homine multo est evidentius, primum ex ea caritate quae est inter natos et parentes, quae dirimi nisi detestabili scelere non potest; deinde cum similis sensus exstitit amoris, si aliquem nacti sumus cuius cum moribus et natura congruamus, quod in eo quasi lumen aliquod probitatis et virtutis perspicere videamur.
				<br>
				Ac ne quis a nobis hoc ita dici forte miretur, quod alia quaedam in hoc facultas sit ingeni, neque haec dicendi ratio aut disciplina, ne nos quidem huic uni studio penitus umquam dediti fuimus. Etenim omnes artes, quae ad humanitatem pertinent, habent quoddam commune vinculum, et quasi cognatione quadam inter se continentur.
			</p>
		</div>
		<div class="block">
			<h2>Test</h2>
		</div>
		<div class="block">
			<h2>Test</h2>
		</div>
	</div>
	<div class="rblocks">
		<h2>Langages informatiques utilisés au cours de la formation :</h2>
		<p><ul>
			<li>
				Java & FXML<br>
				<a href="testmenu.html" class="blockslinks">Présentation JavaFX avec IntelliJ</a>
				<br><br>
			</li>
			<li>
				PHP & HTML/CSS<br>
				<a href="" class="blockslinks">Présentation de CSS3</a><br>
				<a href="" class="blockslinks">Présentation PHP</a>
				<br><br>
			</li>
			<li>
				SQL<br>
				<a href="" class="blockslinks">Pourquoi MariaDB ?</a>
				<br><br>
			</li>
			<li>
				Python<br>
				<a href="" class="blockslinks">Présentation Python</a>
				<br><br>
			</li>
			<li>
				C# - (à venir)<br>
				<a href="" class="blockslinks">Présentation C#</a>
				<br><br>
			</li>
		</ul></p>
	</div>
	<br>
	<div class="rblocks" class="blockslinks">
		<h2>Compétences développées :</h2>
		<p><ul>
			<li>
				Serveurs web Debian, Ubuntu et WAMP<br>
				<a href="" class="blockslinks">Docs Debian & Ubuntu</a>
				<br><br>
			</li>
			<li>
				Mise en place de certificats SSL<br>
				<a href="" class="blockslinks">Docs SSL</a>
				<br><br>
			</li>
			<li>
				Mise en place d'un NextCloud<br>
				<a href="" class="blockslinks">Docs NextCloud sous Debian 9</a>
				<br><br>
			</li>
			<li>
				Mise en place d'un serveur de mail Zimbra<br>
				<a href="" class="blockslinks">Docs Zimbra sous Debian 9</a>
				<br><br>
			</li>
			<li>
				Mise en place d'un serveur de données Samba<br>
				<a href="" class="blockslinks">Docs Samba sous Debian 9</a>
				<br><br>
			</li>
			<li>
				<a href="" class="blockslinks">Tableau de compétences du référentiel</a>
				<br><br>
			</li>
		</ul></p>
	</div>
	<br>
	<div class="rblocks"><h2>Stages :</h2>
		<p>
		<ul>
			<li>
				<a href="" class="blockslinks">Description du stage à l'IFIDE SupFormation</a>
				<br><br>
			</li>
			<li>
				<a href="" class="blockslinks">Descriptif du stage chez Mr Didier Andrès</a>
				<br><br>
			</li>
		</ul>
		</p>
	</div>
	<br>
	<div class="rblocks">
		<p>Block de droite!</p>
	</div>
	<br>
</main>
	<script type="text/javascript">
		var url = "../javascript/test.js";
		$.getScript(url, function() {
			remotecall();
		});
	</script>
	<div id="footer">
		<h2>J'ai des réponses à vos questions...</h2>
		<a href="contact.php" class="links">Contact</a><br>
		<a href="mentions-legales.php" class="links">Mentions légales</a><br>
		<a href="truc.php" class="links">Autres</a><br>
	</div>
</body>


</html>