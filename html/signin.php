<?php
include('head.php');
?>

<body>
	<link rel="stylesheet" type="text/css" href="../css/signin.css">
<div>
	<center><div class="signinform"><h2 id="title">Inscrivez vous !</h2></div>
	<div class="signinform">
		<form id="signinform" method="POST" action="../php/signin.php">
			<input id="login" class="elts" type="text" name="login" placeholder="Login" required="true"><br>
			<input id="password" class="elts" type="password" name="pwd" placeholder="Mot de passe" required="true"><br>
			<input id="pwd" class="elts" type="password" name="password" placeholder="Confirmez votre mot de passe" required="true"><br>
			<input id="mail" class="elts" type="email" name="mail" placeholder="email" required="true"><br>
			<input id="submit" class="sub" type="submit" name="submit" value="Je m'inscris!">
		</form>
	</div></center>
<?php
include('foot.php');
?>
	<script>
		function send() { //ajax
			try {
				return new Promise(function(resolve, reject) {
					var req = new XMLHttpRequest();
					url = "http://192.168.1.75:81/portfolio-anis-mirabet/php/signin.php";
					req.open('POST', url);
					req.setRequestHeader("Content-Type", "application/json");
					req.onreadystatechange = function() {
						if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
							var rep = JSON.parse(this.responseText);
							//var rep = this.responseText;
							document.getElementById("signinform").innerHTML = rep;
							//updateView();
						}
						else if (this.readyState == XMLHttpRequest.DONE && this.status != 200) {
							document.getElementById("signinform").innerHTML = "La requête a été envoyée, mais une erreur est survenue...";
							updateView();
							window.alert("aww...");
						}
						else {
							document.getElementById("signinform").innerHTML = "Une tempête de sable perturbe notre convoi...";
						}
						//document.getElementById("footer").style ="position: fixed; bottom: 0; width: 100%;";
					};
					data = {
						login: document.getElementById("login").value,
						pwd: document.getElementById("pwd").value,
						mail: document.getElementById("mail").value
					};
					jdata = JSON.stringify(data);
					//window.alert(jdata);
					req.send(jdata);
					resolve("true");				
				});
			}
			catch (err) {
				window.alert("erreur 2");
			}
			
		}
		try { //lance l'ajax lorsqu'on envoie le formulaire
			document.getElementById("signinform").addEventListener('submit', function(evt) {
				var password = document.getElementById("password").value;
				var pwd = document.getElementById("pwd").value;
				var result = document.getElementById("signinform");
				if (password != pwd) {
					result.innerHTML = "Les deux mots de passe ne sont pas identiques.";
				}
				else {
					window.alert("OK, je pense..");
					send();
				}
				evt.preventDefault();
				
			});
		}
		catch (er) {
			window.alert("erreur 1");
		}
		document.getElementById("footer").style = "position: fixed; bottom: 0; width: 100%;";
	</script>
</div>
</body>