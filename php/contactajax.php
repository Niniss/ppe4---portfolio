<?php
session_start();
header('CONTENT-TYPE: text/plain; charset=UTF-8');

//DB VARS
$table = "contact";
$column1 = "subject";
$column2 = "content";
$column3 = "mail";

try {
	$data = (array)json_decode(file_get_contents('php://input'));
	//$_SESSION['test'] = $data;
	//print_r($_SESSION['test']);
	include('cnx.php');
	$subject = $data['subject'];
	$message = $data['content'];
	$mail = $data['mail'];
	try {
		$query = $conn->prepare("INSERT INTO " .$table. "(" .$column1. ", " .$column2. ", " .$column3.") VALUES (:subject, :content, :mail)");
		$query->bindParam(':subject', $subject);
		$query->bindParam(':content', $message);
		$query->bindParam(':mail', $mail);
		$query->execute();
		echo json_encode("Merci ! Je prendrai bien soin de votre message comme si c'était le mien, c'est promis !");
	}
	catch (PDOException $error) {
		echo json_encode("Une erreur est survenue, mais ne vous inquietez pas ! Ce n est pas de votre faute ! Reessayez plus tard !".$error);
	}
}
catch (Exception $e) {
	echo json_encode($e);
}
?>