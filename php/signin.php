<?php
session_start();
header('CONTENT-TYPE: text/plain; charset=UTF-8');
$table = "guests";
$output = array();
$checker = "OK";

$data = (array)json_decode(file_get_contents('php://input'));
if (isset($data['login']) && isset($data['pwd']) && isset($data['mail']) && !empty($data['login']) && !empty($data['pwd']) && !empty($data['mail'])) {
	$login = $data['login'];
	$pwd = $data['pwd'];
	$mail = $data['mail'];
	try {
		include('cnx.php');		
		$check = $conn->prepare("SELECT login, mail FROM guests");
		$check->execute();
		$rset0 = $check->fetchAll();
	}
	catch (PDOException $pdoe) {
		echo json_encode("erreur lors de la recuperation des donnees : " .$pdoe."<br>");
	}
	foreach ($rset0 as $r) {
		if ($r['mail'] == $mail) {
			$output['mail'] = "Cette adresse email est deja inscrite. La fonction recuperation de mot de passe n est pas disponible pour le moment.<br>";
			$checker = "KO";
		}
		if ($r['login'] == $login) {
			$output['login'] = "Ce login est deja pris.<br>";
			$checker = "KO";
		}
	}
	if ($checker == "OK") {
		
		try {
			$query = $conn->prepare("INSERT INTO " .$table. " VALUES(:login, :pwd, :mail, '')");
			$query->bindParam(':login', $login);
			$query->bindParam(':pwd', $pwd);
			$query->bindParam(':mail', $mail);
			$query->execute();
			$output['response'] = "Vous avez été inscrit !<br>";

		}
		catch (PDOException $pdoe) {
			echo json_encode("erreur lors de l inscription : ".$pdoe."<br>");
		}
	}
	else {
		$output['response'] = "je ne peux pas vous inscrire dans ces conditions.<br>";
	}	
}
else {
	$output['response'] = "il semblerait que certaines donnees ne nous soient pas parvenues.<br>";
}
$str = "";
foreach ($output as $arr) {
	$str = $str."<br>".$arr;
}
echo json_encode($str);
?>