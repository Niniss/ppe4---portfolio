<?php

// CONNECTION VARIABLES ---------------------------------------------------------------------------------------

date_default_timezone_set("Europe/Paris");
$servername = "localhost";
$dbname="portfolio";
$username = "anis";
$password = "0000";
$charset = "utf8mb4";

// feel free to modify connecting options

$options = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false
];


// CONNECTING TO THE DATABASE	-------------------------------------------------------------------------------
try {
	$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, $options);
	//echo "Connexion à la base de données réussie !<br>";	
	} 	
catch(PDOException $e){
	//echo "Echec de la connexion : " . $e->getMessage();
	}
//	-----------------------------------------------------------------------------------------------------------

?>