<?php
session_start();
?>
<html class="noselect">
<?php
include('head.php');
?>

<body>
<div id="layout">
	<link rel="stylesheet" type="text/css" href="../css/contact.css">
	<center><h1>Vous souhaitez me contacter ?</h1>
	<p class="main">Choisissez l'objet de votre message : 
	<select id="subject" class="elts" name="subject" required="true">
		<option value="contact">Vous êtes un professionel et vous souhaitez me contacter</option>
		<option value="help">Demander de l'aide</option>
		<option value="comment">Commentaire sur le site web</option>
		<option value="sayhi">Juste pour dire coucou</option>
	</select>
	<div id="contact-form" class="main" style="">
		<form id="form">
			<textarea id="content" class="elts" style="height: 120px;" type="text" name="content" required="true" placeholder="Votre message :"></textarea>
			<br><span id="rest">Entrez votre message ci-dessus.</span>
			<br>
			<input id="email" type="email" class="elts" name="email" required="true" placeholder="email :">
			<input id="submit" type="submit" class="elts" name="submit" value="C'est parti, let's go!">
		</form>
	</div></center>
	<h1 id="result"></h1>
	<div id="thanks" style="display: none;">
		<center><p>Merci ! Votre message a été envoyé !</p></center>
	</div>
	<script type="text/javascript">
		//Redimension de l'aire de texte contenant le corps du message
		var textAreas = document.getElementsByTagName('textarea');
		var taHeights = [];
		taLength = textAreas.length;
		try {
			for (i=0; i < taLength; i++) {
				taHeights.push(textAreas[i].style.height);
				textAreas[i].addEventListener('input', function(e) {
					for (j=0;j<taLength;j++) {
						if (e.target.id == textAreas[j].id) {
							var a=j;
						}
					}
					e.target.style.height = taHeights[a];
					e.target.style.height = e.target.scrollHeight+10+'px';
				});
			}
		}
		catch (err) {
			window.alert(err);
		}
		//Nombre de caractères maximum dans input
		try {
			var content = document.getElementById("content");
			var rest = document.getElementById("rest");
			var save ="";
			const maxChar = 360;
			content.addEventListener('input', function(e) {
				var com = "";
				contentValue = e.target.value;
				contentLength = contentValue.length;
				if (contentLength > maxChar) {
					com = save;
					content.value = com;
				}
				else {
					for (i=0; i < contentLength; i++) {
						com = com + contentValue[i];
					}
				}
				save = com;
				if (save == "") {
					rest.innerHTML = "Vous avez changé d'avis? :(";
				}
				else {
					rest.innerHTML = (maxChar - com.length) + " caractères restants.";
				}
				
			});
		}
		catch (e) {
			window.alert("erreur carac");
		}
		// Envoi des données par ajax et affichage du nouveau message

		function updateView() { //fonction d'affichage du nouveau message
			document.getElementById("contact-form").style = "display: none;";
			document.getElementById("thanks").style = "";
		}
		
		function send() { //ajax
			try {
				return new Promise(function(resolve, reject) {
					var req = new XMLHttpRequest();
					url = "http://192.168.0.10:81/portfolio-anis-mirabet/contactajax.php";
					req.open('POST', url, true);
					req.setRequestHeader("Content-Type", "application/json");
					req.onreadystatechange = function() {
						if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
							document.getElementById("result").innerHTML = "Ok!";
							//var rep = JSON.parse(this.responseText);
							var rep = this.responseText;
							updateView();
							try {
								window.alert(rep);
							}
							catch (e) {
								window.alert("aie aie aie");
							}
						}
						else if (this.readyState == XMLHttpRequest.DONE && this.status != 200) {
							window.alert("La requête a été envoyée, mais une erreur est survenue...");
							var rep = this.responseText;
							//var rep = JSON.parse(this.responseText);
							window.alert(rep);
						}
						else {
							window.alert("La requête n'a pas abouti..."+this.status);

						}
					};
					data = {
						subject: document.getElementById("subject").value,
						content: document.getElementById("content").value,
						mail: document.getElementById("email").value
					};
					jdata = JSON.stringify(data);
					//window.alert(jdata);
					req.send(jdata);
					//window.alert("test");
					resolve("true");				
				});
			}
			catch (err) {
				window.alert("erreur 2");
			}
			
		}
		try { //lance l'ajax lorsqu'on envoie le formulaire
			document.getElementById("form").addEventListener('submit', function(evt) {
				evt.preventDefault();
				send();
			});
		}
		catch (er) {
			window.alert("erreur 1");
		}
	</script>
</div>
</body>